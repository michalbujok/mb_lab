#!/bin/bash
# Script that installs MongoDB
# and sets MongoDB congig
# Lukasz Boduch
# 30.01.2019
# version 1.0
#

sudo mkdir -p /data/db
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo service mongod start


# Modify /etc/mongod.conf
#

cat > /etc/mongod.conf << EOF 
# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
 dbPath: /var/lib/mongodb
 journal:
   enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
 destination: file
 logAppend: true
 path: /var/log/mongodb/mongod.log

# network interfaces
net:
 port: 27017
 bindIp: 0.0.0.0


# how the process runs
processManagement:
 timeZoneInfo: /usr/share/zoneinfo

#security:
#  authorization: "enabled"
#operationProfiling:

#replication:

#sharding:

## Enterprise-Only Options:

#auditLog:

#snmp:
EOF


#download and install azcopy
#

wget -O azcopy.tar.gz https://aka.ms/downloadazcopylinux64
sleep 30
tar -xf azcopy.tar.gz
sleep 10
sudo ./install.sh
#this should be done 1st?


# download DB repo from storage blob
#

sudo mkdir /home/casestudy/db
azcopy \
--source https://usatask3storageaccount.blob.core.windows.net/usatask3storageaccountcontainer/project/ucn-azure-casestudy/db/ \
--destination /home/casestudy/db \
--source-key jdpDwmLcN5dCXwPwzeoVNnOUNre1baWuwyoOAOq5V6GWFUvoPxUVx+w2vPcD/Plj4HAX4FEPEjAETdgnbesMvw== \
--recursive


# Import MongoDB database
#

chmod +x /home/casestudy/db/import.sh
cd /home/casestudy/
/home/casestudy/db/import.sh


#END
